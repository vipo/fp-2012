module Main where

import Data.Maybe(fromMaybe)
import Test.Hspec
import Test.QuickCheck
import Data.Serialization.BencodeParsec

main :: IO ()
main = hspec $ do
  describe "Bencoding integer" $ do
    it "decodes a positive integer" $
      decode "i42e" `shouldBe` Just (BInteger(42))
    it "fails on integer w/o ending tag" $
      decode "i43" `shouldBe` Nothing
    it "decodes a negative" $
	  decode "i-54e" `shouldBe` Just (BInteger(-54))

  describe "Bencodeing properties" $ do
    it "works both ways" $
	  property $ \x -> fromMaybe undefined (decode (encode x)) == (x :: BType) 

instance Arbitrary BType where
  arbitrary = do
    depth <- choose (0,10) :: Gen Int
    b <- mkBType depth
    return b

mkString :: Gen BType
mkString = do
  s <- arbitrary
  return (BString s)

mkInteger :: Gen BType
mkInteger = do
  i <- arbitrary
  return (BInteger i)

mkList :: Int -> Gen BType
mkList depth = do
  l <- vectorOf 2 (mkBType depth')
  return (BList l)
  where 
    depth' = depth-1

mkDict :: Int -> Gen BType
mkDict depth = do
  e <- vectorOf 2 (mkDictEntry depth')
  return (BDict e)
  where
    depth' = depth-1

mkDictEntry :: Int -> Gen BDictEntry
mkDictEntry depth = do
  k <- arbitrary :: Gen String
  v <- mkBType depth'
  return (k, v)
  where
    depth' = depth-1

mkBType :: Int -> Gen BType
mkBType 0 = oneof [mkString, mkInteger]
mkBType depth = oneof [mkString, mkInteger, mkList depth, mkDict depth]

