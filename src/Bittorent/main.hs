import Stream
import System.IO
import qualified Data.ByteString as B

main = do
	input <- B.getContents
	let list = B.unpack input
	print $ receive list