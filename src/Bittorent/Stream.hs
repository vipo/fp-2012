module Stream ( Message, receive, send ) where

import Data.Int
import Data.Word
import Data.List
import Data.Bits

data Message =
	Choke |
	Unchoke |
	Interested |
	NotInterested |
	Have Int32 |
	Bitfield [Bool] |
	Request Int32 Int32 Int32 |
	Piece Int32 Int32 [Word8] |
	Cancel Int32 Int32 Int32 |
	Undef deriving Show


----- Helpers -----
splitEvery :: Int -> [a] -> [[a]]
splitEvery n = takeWhile (not.null) . map (take n) . iterate (drop n)


----- Int32 converters -----
toInt32 :: [Word8] -> Int32
toInt32 list = foldl (\ a b -> a * 256 + fromIntegral b ) 0 $ take 4 list

toInt32' :: [Word8] -> Int -> Int32
toInt32' list x = toInt32 $ drop (x * 4) list

fromInt32 :: Int32 -> [Word8]
fromInt32 int = foldl (\ a b -> ( floor $ (fromIntegral int) / (256^b) ):a ) [] [0..3]


----- Bitfield converters -----
toBitList :: Word8 -> [Bool]
toBitList x = reverse $ foldl (\ a b -> (testBit x b):a ) [] [0..7]

toBitfield :: [Word8] -> [Bool]
toBitfield list = foldl (\ a b -> a ++ (toBitList b) ) [] list

fromBitList :: [Bool] -> Word8
fromBitList bf = foldl (\ a (b,i) -> if b then setBit a i else clearBit a i ) 0 $ zip bf [0..7]

fromBitfield :: [Bool] -> [Word8]
fromBitfield bf = foldl (\ a b -> a ++ [(fromBitList b)] ) [] $ splitEvery 8 bf


-- Try testing below piece of code by running:
--     receive $ send (<message>)
--     send $ receive [<bytes>]
-- You should get the same output as input

----- These pack stream into an object -----
receive :: [Word8] -> Message
receive (0:xs) = Choke
receive (1:xs) = Unchoke
receive (2:xs) = Interested
receive (3:xs) = NotInterested
receive (4:xs) = Have $ toInt32 xs
receive (5:xs) = Bitfield $ toBitfield xs
receive (6:xs) = Request ( toInt32' xs 0 ) ( toInt32' xs 1 ) ( toInt32' xs 2 )
receive (7:xs) = Piece ( toInt32' xs 0 ) ( toInt32' xs 1 ) $ drop 8 xs
receive (8:xs) = Cancel ( toInt32' xs 0 ) ( toInt32' xs 1 ) ( toInt32' xs 2 )
receive (_:xs) = Undef

----- These unpack object into a stream -----
send :: Message -> [Word8]
send (Choke)         = [0]
send (Unchoke)       = [1]
send (Interested)    = [2]
send (NotInterested) = [3]
send (Have x)        = [4] ++ fromInt32 x
send (Bitfield x)    = [5] ++ fromBitfield x
send (Request a b c) = [6] ++ fromInt32 a ++ fromInt32 b ++ fromInt32 c
send (Piece a b c)   = [7] ++ fromInt32 a ++ fromInt32 b ++ c
send (Cancel a b c)  = [8] ++ fromInt32 a ++ fromInt32 b ++ fromInt32 c
