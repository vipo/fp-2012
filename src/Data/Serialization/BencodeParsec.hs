module Data.Serialization.BencodeParsec where

import Text.ParserCombinators.Parsec
import Data.List

type BDictEntry = (String, BType)

data BType = BString String
	| BInteger Integer
	| BDict [BDictEntry]
	| BList [BType]
	deriving (Show, Eq, Ord)

parseBType :: Parser BType
parseBType = parseBInt <|> parseBList <|> parseBDict <|> parseBStr

parseBInt = string "i" >> parseInt >>= (\v -> string "e" >> return v)
parseInt = (string "-" >> parseInt >>= (\(BInteger i) -> return (BInteger (-i))))
	<|> (many1 digit >>= (\v -> return (BInteger (read v))))

parseBStr = do l <- many1 digit; string ":" ; s <- count (read l) anyChar; return (BString s)

parseBList = do string "l"; l <- many parseBType; string "e"; return (BList l)
parseBDict = do string "d"; e <- many parseBDictEntry; string "e"; return (BDict e)
parseBDictEntry = do (BString k) <- parseBStr; v <- parseBType; return (k, v)

decode e = case parse parseBType "" e of
  Left _ -> Nothing
  Right x -> Just x

encode :: BType -> String
encode (BString s) = show (length s) ++ ":" ++ s
encode (BInteger i) = "i" ++ show i ++ "e"
encode (BList l) = "l" ++ foldl (\ a b -> a ++ encode b) "" l ++ "e"
encode (BDict d) = "d" ++ foldl (\ a (k, b) -> a ++ encode (BString k) ++ encode b) "" (sort d) ++ "e"


