module Data.Serialization.Bencode (BType, decode) where

import Data.Char(ord)
import Data.Word
import Data.List(sort, sortBy)
import qualified Codec.Binary.UTF8.String as UTF8

type BDictEntry = (String, BType)

data BType = BString (String, [Word8])
	| BInteger Integer
	| BDict [BDictEntry]
	| BList [BType]
	deriving Show

instance Eq BType where
	BString (_, a) == BString (_, b) = a == b
	BInteger a == BInteger b = a == b
	BList a == BList b = a == b
	BDict a == BDict b = sortBy f a == sortBy f b
		where f a b = compare (fst a) (fst b)

class Encoder a where
	encodeToBytes :: a -> [Word8]
	encodeToChars :: a -> String
	encodeToChars a = UTF8.decode $ encodeToBytes a

class Decoder a where
	decodeBytes :: [Word8] -> (a, [Word8])
	decodeChars :: String -> a
	decodeChars a = fst $ decodeBytes $ UTF8.encode a

instance Decoder BType where 
	decodeBytes = decode

instance Encoder BType where
	encodeToBytes (BInteger x) = [105] ++ UTF8.encode (show x) ++ [101]
	encodeToBytes (BString (_, b)) = (UTF8.encode $ show $ length b) ++ [58] ++ b
	encodeToBytes (BList x) = [108] ++ (foldl f [] x ) ++ [101] 
		where f a e = a ++ encodeToBytes e
	encodeToBytes (BDict x) = [100] ++ (foldl f [] x ) ++ [101]
		where f a (k,v) = a ++ (UTF8.encode $ show $ length $ UTF8.encode k) ++ [58] ++ 
			UTF8.encode k ++ encodeToBytes v

decode (100:xs) = decodeDict xs         --'d'
decode (105:xs) = decodeInt xs          --'i'
decode (108:xs) = decodeList xs         --'l'
decode x = decodeString x

makeBString :: String -> BType
makeBString str = BString (str, UTF8.encode str)

decodeDict :: [Word8] -> (BType, [Word8])
decodeDict x = decodeDict' x []

decodeDict' (101:xs) acc = (BDict $ reverse $ acc, xs)	--'e'
decodeDict' [] _ = (BDict [], [])
decodeDict' x acc = decodeDict' left $ (key,val):acc
	where 	(val, left)				= decode keyLeft;
			(key, bytes, keyLeft)	= decodeRawString x

decodeList x = decodeList' x [] 

decodeList' (101:xs) acc = (BList $ reverse $ acc, xs)	--'e'
decodeList' [] _ = (BList [], [])
decodeList' x acc = decodeList' left $ val:acc
	where (val, left) = decode x

decodeInt x = (BInteger val , drop 1 left)
   where (val, left) = parseInt x

decodeString x = (BString (utf8, bytes), left)
	where (utf8, bytes, left) = decodeRawString x

decodeRawString x = 
	let		(length, intLeft) = parseInt x
			(val, stringLeft) = parseString length intLeft
	in (UTF8.decode val, val, stringLeft)

parseString :: Integer -> [Word8] -> ([Word8], [Word8])
parseString length (58:xs) = (take (fromIntegral length) xs, -- ':'
							   drop (fromIntegral length) xs)
parseString _ _ = ([], [])

parseInt :: [Word8] -> (Integer, [Word8])
parseInt x = parseInt' x 0 True

parseInt' :: [Word8] -> Integer -> Bool -> (Integer, [Word8])
parseInt' [] v _ = (v, [])
parseInt' a@(x:xs) v sign
	| x >= 48 && x <= 57 	= parseInt' xs (v*10 + (fromIntegral (x-48))) sign	-- 0..9
	| x == 45				= parseInt' xs 0 False 								-- '-'
	| otherwise				= case sign of 
								True 	-> ( v, a)
								False	-> (-v, a)

